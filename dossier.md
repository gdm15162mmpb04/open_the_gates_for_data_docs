# Dossier   
##### Mathias Fonck & Thomas Van Der Sypt
## Datasets

[Countries](http://api.worldbank.org/countries/all)

[Start-up procedures to register a business (number)](http://data.worldbank.org/indicator/IC.REG.PROC)

[Time required to start a business (days)](http://data.worldbank.org/indicator/IC.REG.DURS)

[New businesses registered (number)](http://data.worldbank.org/indicator/IC.BUS.NREG)

[New business density (new registrations per 1,000 people ages 15-64)](http://data.worldbank.org/indicator/IC.BUS.NDNS.ZS)

[Real interest rate %](http://data.worldbank.org/indicator/FR.INR.RINR)

[Inflation, GDP deflator (annual %)](http://data.worldbank.org/indicator/NY.GDP.DEFL.KD.ZG)

## Briefing, analyse, concept
Upstart is een app die je opweg helpt om je eerste bedrijf op te richten. De app kan je vertellen hoeveel legale procedures er nodig zijn om je bedrijf op te richten per land.
Evenals het aantal nieuwe bedrijven in de regio, intrest per land en inflatie per land. Upstart heeft ook een startup ranking. Hierbij worden succesvolle startups met elkaar gemeten 
en in een high score geplaatst.

## Functionele specificaties
De gebruiker selecteerd een land, vervolgens wordt er allerlei informatie over dit land gevisualiseerd. 
* De densiteit van startups in het geselecteerde land visualiseren via google maps. 
* Zuivere data weergeven bijvoorbeeld: het aantal procedures dat vereist is om een bedrijf op te richten (in dat geselecteerde land)
* Rankings van de startups
* dynamisch ingeladen data
* single page application
* responsive
* Google Maps 


## Technische specificaties
* HTML 5, CSS3, JavaScript
* Handlebars
* jQuery

## Persona's
![Persona 1](images/persona/persona1.png)  
![Persona 2](images/persona/persona2.png)  
![Persona 3](images/persona/persona3.jpg)  

## Moodboards
![Moodboard 1](images/moodboards/moodboards-01.png)  
![Moodboard 2](images/moodboards/moodboards-02.png)  
![Moodboard 3](images/moodboards/moodboards-03.png)  

## Sitemap
![Sitemap](images/sitemap/sitemap.png)

## Wireframes
![Wireframes mobile](images/wireframes/wireframes-01.png)  
![Wireflow](images/wireframes/wireflow.png)  
![Wireframes tablet](images/wireframes/wireframes-02.png)  
![Wireframes desktop](images/wireframes/wireframes-03.png)  
![Wireframes mobile 2](images/wireframes/wireframes-04.png)  
![Wireframes tablet 2](images/wireframes/wireframes-05.png)  
![Wireframes desktop 2](images/wireframes/wireframes-06.png)

## Style Tiles
![Style Tile 1](images/style tiles/style_tiles-01.png)  
![Style Tile 2](images/style tiles/style_tiles-02.png)  
![Style Tile 3](images/style tiles/style_tiles-03.png)

## Visual Designs
![Visual Design 1](images/visual designs/visual_designs-01.png)  
![Visual Design 2](images/visual designs/visual_designs-02.png)

## Screenshots
### Screenshots eindresultaat
![Screenshot eindresultaat header + jumbotron](images/screenshots/design/screenshot-eindresultaat-01.png)  
![Screenshot eindresultaat segments](images/screenshots/design/screenshot-eindresultaat-02.png)  
![Screenshot eindresultaat 3](images/screenshots/design/screenshot-eindresultaat-03.png)  
![Screenshot eindresultaat google maps](images/screenshots/design/screenshot-eindresultaat-04.png)  
![Screenshot eindresultaat typography](images/screenshots/design/screenshot-eindresultaat-05.png)  
![Screenshot eindresultaat navigation](images/screenshots/design/screenshot-eindresultaat-06.png)  
![Screenshot eindresultaat 404 page](images/screenshots/design/screenshot-eindresultaat-07.png)  

### Screenshots snippets
![Screenshot html snippet 1](images/screenshots/code snippets/snippet-html-01.jpg)  
![Screenshot html snippet 2](images/screenshots/code snippets/snippet-html-02.jpg)  
![Screenshot html snippet 3](images/screenshots/code snippets/snippet-html-03.jpg)  
![Screenshot html snippet 4](images/screenshots/code snippets/snippet-html-04.jpg)  
![Screenshot html snippet 5](images/screenshots/code snippets/snippet-html-05.jpg)  
![Screenshot html snippet 6](images/screenshots/code snippets/snippet-html-06.jpg)  

![Screenshot css snippet 1](images/screenshots/code snippets/snippet-css-01.jpg)  
![Screenshot css snippet 2](images/screenshots/code snippets/snippet-css-02.jpg)  
![Screenshot css snippet 3](images/screenshots/code snippets/snippet-css-03.jpg)  
![Screenshot css snippet 4](images/screenshots/code snippets/snippet-css-04.jpg)  
![Screenshot css snippet 5](images/screenshots/code snippets/snippet-css-05.jpg)  
![Screenshot css snippet 6](images/screenshots/code snippets/snippet-css-06.jpg)  

![Screenshot js snippet 1](images/screenshots/code snippets/snippet-js-01.jpg)  
![Screenshot js snippet 2](images/screenshots/code snippets/snippet-js-02.jpg)  
![Screenshot js snippet 3](images/screenshots/code snippets/snippet-js-03.jpg)  
![Screenshot js snippet 4](images/screenshots/code snippets/snippet-js-04.jpg)  
![Screenshot js snippet 5](images/screenshots/code snippets/snippet-js-05.jpg)  
![Screenshot js snippet 6](images/screenshots/code snippets/snippet-js-06.jpg)  
![Screenshot js snippet 7](images/screenshots/code snippets/snippet-js-07.jpg)  
![Screenshot js snippet 8](images/screenshots/code snippets/snippet-js-08.jpg)  
![Screenshot js snippet 9](images/screenshots/code snippets/snippet-js-09.jpg)  
![Screenshot js snippet 10](images/screenshots/code snippets/snippet-js-10.jpg)  
![Screenshot js snippet 11](images/screenshots/code snippets/snippet-js-11.jpg)  
![Screenshot js snippet 12](images/screenshots/code snippets/snippet-js-12.jpg)  
![Screenshot js snippet 13](images/screenshots/code snippets/snippet-js-13.jpg)  

