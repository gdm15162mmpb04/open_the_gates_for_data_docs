#Upstart

## Concept
Upstart is een app die je opweg helpt om je eerste bedrijf op te richten. De app kan je vertellen hoeveel legale procedures er nodig zijn om je bedrijf op te richten per land.
Evenals het aantal nieuwe bedrijven in de regio, intrest per land en inflatie per land. Upstart plaatst heeft ook een startup ranking. Hierbij worden succesvolle startups met elkaar gemeten 
en in een high score geplaatst.

## Functionele specificaties
Upstart is responsive en werkt met een grid-systeem.